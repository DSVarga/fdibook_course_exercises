%% Setup of LTI input-output models with additive faults
clear variables
% setup the system with faults [Gu(s) Gd(s) Gf(s)]
s = tf('s'); % define the Laplace variable s
mu = 1; md = 1; p = 2;            % set dimensions
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gd = [(s-1)/(s+2); 0];            % enter Gd(s)
Gf = [Gu eye(p)];                 % enter Gf(s)

% set input groups for [Gu(s) Gd(s) Gf(s)]
Inputs = struct('c',1:mu,'d',mu+(1:md),'f',1:mu,'fs',1:p);

% build state-space realization of [Gu(s) Gd(s) Gf(s)]
sysf = fdimodset(ss([Gu Gd]),Inputs)

%% Setup of LTI state-space models with additive fault
clear variables
% generate LTI state-space system (A,B,C,D) with
% n = 3 states, m = 3 inputs and p = 3 outputs
n = 3; m = 3; p = 3; syss = rss(n,p,m);

% define control and disturbance input dimensions
mu = 2; md = 1;  % B = [ Bu  Bd ] and D = [ Du  Dd ]

% define input groups for [ Bu  Bd Bf ] and D = [ Du  Dd Df ]
Inputs = struct('c',1:mu,'d',mu+(1:md),'f',1:mu,'fs',1:p);

% build system with additive faults
sysf = fdimodset(syss,Inputs)

%% Setup of multiple LTI input-output models
clear variables
% Generation of a multiple model for actuator faults
s = tf('s');
k1 = 14; % nominal gain
sys(:,:,1) = ss(k1/(s+k1));           % nominal case
sys(:,:,2) = ss(0.5*k1/(s+0.5*k1));   % 50% LOE
sys(:,:,3) = ss(10*k1/(s+10*k1));     % disconnection
sys(:,:,4) = ss(0.01*k1/(s+0.01*k1)); % stall load

% set input group for 'controls'
sys = mdmodset(sys,struct('c',1))

%% Setup of multiple LTI state-space models
clear variables
% Lateral aircraft dynamics model (without faults)
% n = 4 states, mu = 2 control inputs
% p = 4 measurable outputs
A = [-.4492  0.046  0.0053  -0.9926;
     0       0      1        0.0067;
    -50.8436 0     -5.2184   0.722;
    16.4148  0      0.0026  -0.6627];
Bu = [.0004 .0011; 0 0; -1.4161 .2621; -.0633 -.1205];
C = eye(4); p = size(C,1); mu = size(Bu,2);
% LOE faults as input scaling gains: \Gamma^{(j)}
Gamma = 1 - [ 0 0  0 .5 .5 .5 1 1  1;
              0 .5 1 0  .5 1  0 .5 1 ]';
N = size(Gamma,1); % number of LOE cases
% define j-th fault model: Gu(s)*diag(Gamma(j,1))
sys = ss(zeros(p,mu,N,1));
for j = 1:N
    sys(:,:,j,1) = ss(A,Bu*diag(Gamma(j,:)),C,0);
end
% set input group 'controls'
sys = mdmodset(sys,struct('c',1:mu))

%% Recasting a LPV model into a LTI model with noise inputs
clear variables
% define rho1 and rho2 as uncertain parameters
r1 = ureal('rho1',0); r2 = ureal('rho2',0);
% define A(rho1,rho2), Bu, C, Du
n = 3; mu = 2; p = 2;       % enter dimensions
A = [ -.8 0 0;
      0 -0.5*(1+r1) 0.6*(1+r2);
      0 -0.6*(1+r2) -0.5*(1+r1) ];
Bu = [1 1; 1 0; 0 1];
C = [0 1 1;1 1 0]; Du = zeros(p,mu);

% build S(rho)
S = [ A Bu; C Du];
% compute the elements of LFT-based representation
% S(rho) =  M22 + M21*Delta*(I-Delta*M11)^{-1}M12
[M,Delta] = lftdata(S); Delta
nd = size(Delta,1);              % size of Delta

% compute an orthogonal basis U1 for the range of M21
U1 = orth(M(nd+1:end,1:nd));

% compute Bw and Dw
mw = size(U1,2);
Bw = U1(1:n,:); Dw = U1(n+1:end,:);

% setup plant model with noise inputs
sys = ss(A.NominalValue,[Bu Bw],C,[Du Dw]);

% define input groups 'controls', 'noise' and 'faults'
Inputs = struct('c',1:mu,'n',mu+(1:mw),'f',1:mu);
sysf = fdimodset(sys,Inputs)


%% Recasting a multiple LTI state-space model into a LTI model with noise inputs
clear variables
% generate (Ai,Bu,C,Du) for i = 1, ..., N
n = 3; mu = 2; p = 2;         % dimensions
N = 2;                        % number of models
r = [zeros(1,2); rand(1,2)];  % grid values
% enter constant matrices only once
Bu = [1 1; 1 0; 0 1];
C = [0 1 1;1 1 0]; Du = zeros(p,mu);
sysm(:,:,N,1) = ss(Du);  % allocate multiple model
for i = 1:N
    r1 = r(i,1); r2 = r(i,2);  % pick a value
    A = [ -.8 0 0; 
          0 -0.5*(1+r1) 0.6*(1+r2);
          0 -0.6*(1+r2) -0.5*(1+r1) ];
    sysm(:,:,i,1) = ss(A,Bu,C,Du);
end   

% build S0 and DeltaS
S0 = [sysm(:,:,1,1).a Bu; C Du]; 
DeltaS = zeros(n+p,0);
for i = 2:N
    DeltaS = [DeltaS [sysm(:,:,i,1).a Bu; C Du]-S0];
end

% compute an orthogonal basis U1 for the range of DeltaS
U1 = orth(DeltaS);

% compute Bw and Dw
mw = size(U1,2);
Bw = U1(1:n,:); Dw = U1(n+1:end,:);

% setup plant model with noise inputs
sys = ss(sysm(:,:,1,1).a,[Bu Bw],C,[Du Dw]);

% define input groups 'controls', 'noise' and 'faults'
Inputs = struct('c',1:mu,'n',mu+(1:mw),'f',1:mu);
sysf = fdimodset(sys,Inputs)
