%% Minimal descriptor realizations
% check G^T(1/z)-G^T(1/z) = 0
clear variables
sys = drss(3,2,3);
sysd = sys'-sys';
% perform minimal realization with minreal
minreal(sysd,1.e-7)  % does not produce 0 (CST limitation)
% perform minimal realization with gminreal
gminreal(sysd,1.e-7) % correct result 0
%%
% check G^{-1}(s)*G(s) = I
sys = ss(tf(1,[1 1]));  %  sys = 1/(s+1)
sysd = sys\sys-1
% compute minimal realization with minreal
minreal(sysd,1.e-7)   % does not produce 0 (CST limitation)
% compute minimal realization with gminreal
gminreal(sysd,1.e-7)  % correct result 0

%%
% define the 2-by-2 improper G(s)
s = tf('s'); % define the complex variable s
G = [s^2 s/(s+1); 0 1/s]

% build a LTI descriptor realization of G(s)
sys = ss(G)
n = order(sys) % order = 5

% the realization is actually minimal;
% checking minimality with minreal fails
%
try
    minreal(sys);
catch err
    err
end

% we can use the function gminreal instead
sysmr = gminreal(sys);
nm = order(sysmr)  % order = 5

%% System analysis
clear variables
% define the 2-by-2 improper G(s)
s = tf('s'); % define the complex variable s
G = [s^2 s/(s+1); 0 1/s];
sys = ss(G); % build a LTI realization of G(s)

% computation of poles and zeros
pole(sys)   %  computes only the finite poles
tzero(sys)  %  computes only the finite zeros
gpole(sys)  %  computes all poles (finite and infinite)
gzero(sys)  %  computes all zeros (finite and infinite)


%% Nullspace computation
clear variables
% Kailath (1980), page 459: rank 2 matrix
s = tf('s');
G = [1/s 0 1/s s;
    0 (s+1)^2 (s+1)^2 0;
    -1 (s+1)^2 s^2+2*s -s^2];
sys = gir(ss(G),1.e-7);

% set options for pole assignment
options = struct('tol',1.e-7,'poles',-1);
% compute a simple left nullspace basis Nl(s)
[Nl,info] = glnull(sys,options);
minreal(tf(Nl))

% check the nullspace condition Nl(s)*G(s) = 0
gminreal(Nl*sys,1.e-7)

%%
% Example 5.3 of (V,2017) (modified)
s = tf('s'); % define the Laplace variable s
mu = 1; md = 1; p = 2; mf = mu+p; % set dimensions
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gd = [(s-1)/(s+2); 0];            % enter Gd(s)
Gf = [Gu eye(p)];                 % enter Gf(s)

% compute a left nullspace basis Q(s) of [Gu(s) Gd(s); I 0]
Q = glnull(ss([Gu Gd;eye(mu,mu+md)]));

% compute Rf(s) = Q(s)*[Gf(s);0]
Rf = gir(Q*[Gf;zeros(mu,mf)]);

% check solvability of the EFD: Rf_j(s) ~= 0 for all j
evalfr(Rf,rand)   % EFD not solvable: Rf_2(s) = 0

%%
% alternative one-step computation
% compute Q(s) and Rf(s) simultaneously in QRf
opt = struct('m2',mf);
QRf = glnull(ss([Gu Gd Gf; eye(mu,mu+md+mf)]),opt);

% compute Rf(s) = Q(s)*[Gf(s);0]
Q = QRf(:,1:mu+p); Rf = QRf(:,mu+p+1:end);

% check solvability of the EFD: Rf_j(s) ~= 0 for all j
evalfr(Rf,rand)   % EFD not solvable: Rf_2(s) = 0


%% Stabilization using coprime factorizations
clear variables
% LCF computation example
s = tf('s');  % define the complex variable s
G = [s^2 s/(s+1); 0 1/s];  % enter G(s)
sys = ss(G);
gpole(sys)    % the system is unstable and improper

% compute the LCF G(s) = M^{-1}(s)*N(s)
opt = struct('poles',[-1 -2 -3], ...
             'mininf',true,'mindeg',true);
[N,M] = glcf(sys,opt); zpk(N), zpk(M)

% check the factorization
norm(gminreal(M*sys-N,1.e-7),inf) %\|M(s)*G(s)-N(s)\|_inf = 0
gpole(M), gpole(N) % check poles
gzero(gir([N M]))  % check coprimeness (no zeros)

%%
% Nullspace-based synthesis example (Procedure EFD)
% Example 5.3 of (V,2017)
s = tf('s'); % define the Laplace variable s
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gd = [(s-1)/(s+2); 0];            % enter Gd(s)
Gf = [Gu [0;1]];                  % define Gf(s)
mu = 1; md = 1; p = 2; mf = mu+1; % set dimensions

% Step 1: compute Q(s) and Rf(s) simultaneously in QRf
opt = struct('m2',mf);
QRf = glnull(ss([Gu Gd Gf; eye(mu,mu+md+mf)]),opt);
% check solvability of the EFD: Rf_j(s) ~= 0 for all j
evalfr(QRf(:,mu+p+1:end),rand)     % EFD solvable

% Step 3: enforce stability degree -3 for Q and Rf
QRf = glcf(QRf,struct('smarg',-3,'sdeg',-3));
% normalize Q and Rf to match example
QRf = QRf/dcgain(QRf(1,end)); 
% extract Q and Rf
Q = QRf(:,1:mu+p); Rf = QRf(:,mu+p+1:end);
minreal(tf(Q)), minreal(tf(Rf))


%% Solving exact model-matching problems
clear variables
% Least order left inverse computation example
% Wang and Davison Example (1973)
s = tf('s');
g = [ s+1 s+2; 
      s+3 s^2+2*s; 
      s^2+3*s 0 ]/(s^2+3*s+2); f = eye(2);
sysg = gir(ss(g)); sysf = ss(f);

% compute a least order solution of X(s)*G(s) = I
[sysx,info] = glsol(sysg,sysf,struct('mindeg',true)); 
gpole(sysx)   % the left inverse is unstable
info.nl       % number of maximally assignable poles

% check solution
gir(sysx*sysg-sysf,1.e-7)

%%
% Stable left inverse computation example
% Wang and Davison Example (1973)
s = tf('s');
g = [ s+1 s+2;
      s+3 s^2+2*s;
      s^2+3*s 0 ]/(s^2+3*s+2); f = eye(2);
sysg = gir(ss(g)); sysf = ss(f);

% compute a stable solution of X(s)*G(s) = I 
% by assigning the maximal number (=3) of poles
sysx = glsol(sysg,sysf,struct('poles',[-1 -2 -3]));
gpole(sysx)   % the left inverse is stable

% check solution 
gir(sysx*sysg-sysf,1.e-7) % use tolerance 1.e-7 


%% 
% Model-matching-based synthesis example
clear variables
% Example 5.3 of (V,2017)
s = tf('s'); % define the Laplace variable s
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gd = [(s-1)/(s+2); 0];            % enter Gd(s)
Gf = [Gu [0;1]];                  % define Gf(s)
mu = 1; md = 1; p = 2; mf = mu+1; % set dimensions
Mr = [(s+2)/(s+3) 1];             % enter Mr(s)

% solve Q(s)*Ge(s) = Fe(s) with
% Ge(s) = [Gu(s) Gd(s) Gf(s);I  0  0] and Fe(s) = [0  0  Mr(s)]
opt = struct('mindeg',true);
Ge = [Gu Gd Gf; eye(mu,mu+md+mf)]; 
Fe = [zeros(1,mu+md) Mr];
Q = glsol(ss(Ge),ss(Fe),opt);

% check solution
gir(Q*Ge-Fe)
