%% Example - Solution of an EFDP
% model setup
s = tf('s'); % define the Laplace variable s
% define Gu(s), Gd(s), Gf(s) = [Gu(s) [0;1]]
Gu = [(s+1)/(s-2); (s+2)/(s-3)];     % enter Gu(s)
Gd = [(s-1)/(s+2); 0];               % enter Gd(s)
p = 2; mu = 1; md = 1; mf = 2;       % set dimensions

% set input groups
Inputs = struct('c',1:mu,'d',mu+(1:md),...
                'f',1:mu,'fs',2);

% build synthesis model with additive faults
sysf = fdimodset(ss([Gu Gd]),Inputs)

% call of EFDSYN with the option 
% for stability degree -3 
[Q,Rf] = efdsyn(sysf,struct('sdeg',-3));
tf(Q), tf(Rf)  % display the resulting Q(s) and Rf(s) 

% check synthesis results: R(s) := Q(s)*Ge(s) = [ 0  0  Rf(s)], 
% with Ge(s) = [Gu(s) Gd(s)  Gf(s);I  0 0]
norm_rez = fdimmperf(Q * [sysf;eye(mu,mu+md+mf)],Rf)

% check weak and strong fault detectability
S_weak = fditspec(Rf)
S_strong = fdisspec(Rf)

% performance evaluation
fscond = fdifscond(Rf) % fault sensitivity condition

% evaluate step responses from the fault inputs
set(Rf,'InputName',{'f1','f2'},'OutputName','r');
step(Rf); ylabel('')
title('Step responses from the fault inputs')

%% Example for computation of achievable specifications
clear variables
% Example of Yuan et al. IJC (1997)
% model setup
p = 3; mu = 1; mf = 8;
A = [ -1 1 0 0; 
       1 -2 1 0; 
       0 1 -2 1; 
       0 0 1 -2 ]; 
Bu = [1 0 0 0]';
Bf = [ 1 0 0 0 1 0 0 0; 
       0 1 0 0 -1 1 0 0; 
       0 0 1 0 0 -1 1 0; 
       0 0 0 1 0 0 -1 1];
C = [ 1 0 0 0; 0 0 1 0; 0 0 0 1];
Du = zeros(p,mu); Df = zeros(p,mf);
% setup the model with additive faults
sysf = ss(A,[Bu Bf],C,[Du Df]);
% set input groups 
Inputs = struct('c',1:mu,'f',mu+(1:mf)); 
sysf = fdimodset(sysf,Inputs)

% compute the achievable weak specifications
optweak = struct('tol',1.e-7,'FDTol',1.e-5);
S_weak = fdigenspec(sysf,optweak), size(S_weak)

% compute the achievable strong specifications 
% for constant faults
optstrong = struct('tol',1.e-7,'FDTol',0.0001,...
      'FDGainTol',.001,'FDFreq',0,'sdeg',-0.05);
S_strong = fdigenspec(sysf,optstrong), size(S_strong)

% select all strong specifications containing 
% at least two zeros
S_strong(sum(S_strong,2)<=6,:)

% select all strong specifications containing
% exactly two zeros
S_strong(sum(S_strong,2)==6,:)

% check that weak specifications include 
% strong specifications
ismember(S_strong,S_weak,'rows')

%% Example for solving an EFDIP
clear variables
% Example - Solution of an EFDIP
% enter output and fault vector dimensions
p = 3; mf = 3;   
% generate random dimensions for state & input vectors
nu = floor(1+4*rand); mu = floor(1+4*rand);
nd = floor(1+4*rand); md = floor(1+4*rand);
% define random Gu(s) and Gd(s) with triplex sensor redundancy 
Gu = ones(3,1)*rss(nu,1,mu); % enter Gu(s) 
Gd = ones(3,1)*rss(nd,1,md); % enter Gd(s) 

% set input groups for Gf(s) representing triplex sensor faults
Inputs = struct('c',1:mu,'d',mu+(1:md),'fs',1:3); 
sysf = fdimodset([Gu Gd],Inputs)


% select S{FDI}
S = fdigenspec(sysf); SFDI = S(sum(S,2)==2,:)

% set options for least order synthesis with EFDISYN
options = struct('tol',1.e-7,'SFDI',SFDI);
[Qt,Rft] = efdisyn(sysf,options);

% normalize Q(s) and Rf(s) to match example
sc = sign([Rft{1}.d(1,2) Rft{2}.d(1,3) Rft{3}.d(1,1)]);
Q = vertcat(Qt{:}); Rf = vertcat(Rft{:});
Q = diag(sc)*Q; Rf = diag(sc)*Rf;
inpnames = [strseq('y',1:p');strseq('u',1:mu)]; 
outnames = strseq('r',1:size(SFDI,1)); 
fnames = strseq('f',1:mf);
Q  = set(Q,'InputName',inpnames,'OutputName',outnames)
Rf = set(Rf,'InputName',fnames,'OutputName',outnames)           

% check synthesis results: R(s) := Q(s)*Ge(s) = [ 0  0  Rf(s)],
% with Ge(s) = [Gu(s) Gd(s)  Gf(s);I  0 0]
R = gir(Q * [sysf;eye(mu,mu+md+mf)]);
norm_rez = fdimmperf(R,Rf)

% check fault isolability
isequal(SFDI,fdisspec(Rf))

% performance evaluation: compute the fault sensitivity conditions 
% for each filter for constant faults (frequency = 0)
fscond = fdifscond(Rf,0,SFDI)

% evaluate step responses
step(Rf); 
title('Step responses from the fault inputs') 
ylabel('Residuals')

%% Example for solving an EFDIP using EFDSYN
clear variables
% Example: (Xiong & Saif 2000, IJRNC)
n = 4; mu = 2; md = 2; mf = 2; p = 4;
a = [ -9.9477 -0.7476 0.2632 -5.0337
      52.1659 2.7452 5.5532 -24.4221
      26.0922 2.6361 -4.1975 -19.2774
      0.0 0.0 1.0 0.0];
bu = [ 0.4422 0.1761
       3.5446 -7.5922
        -5.52 4.49
        0.0 0.0];
bd = [ 0 0; 0 1 ; 1 0; 0 0 ]; 
c = [ 1 0 0 0; 0 1 0 0; 0 0 1 0; 0 1 1 1];
dd = zeros(n,md); du = zeros(n,mu); 
Inputs = struct('c',1:mu,'d',mu+(1:md),'f',1,'fs',3); 
sysf = fdimodset(ss(a,[bu bd],c,[du dd]),Inputs)

% select S{FDI}
opt = struct('tol',1.e-7,'FDGainTol',.01,...
             'FDFreq',0,'sdeg',-0.05);
S_strong = fdigenspec(sysf,opt)

% select specifications containing one zero
SFDI = sortrows(S_strong(sum(S_strong,2)==1,:),2)

nb = size(SFDI,1); % number of specifications
Q = ss(zeros(0,p+mu)); Rf = ss(zeros(0,mf));
options = struct('tol',1.e-7,'sdeg',-5,'smarg',-0.5,...
         'FDFreq',0,'FDGainTol',0.0001,'rdim',1);
for i = 1:nb
   % redefine input groups; use auxiliary inputs to 
   % obtain the i-th row block of  Rf(s) directly
   indd = find(SFDI(i,:) == 0); 
   indf = find(SFDI(i,:) ~= 0);
   Inputsi = struct('c',1:mu,'d',[mu+(1:md) mu+md+indd],...
      'f',mu+md+indf,'aux',mu+md+(1:mf));
   sysc = fdimodset(sysf,Inputsi);
   %  solve an EFDP 
   [Qi,Ri] = efdsyn(sysc,options); 
   Q = [Q; Qi]; order(Qi)
   Rf = [Rf; Ri(:,'aux')];
end
Rf.InputGroup.faults = Rf.InputGroup.aux;

% check synthesis results: R(s) := Q(s)Ge(s) = [0 0 Rf(s)],
% with Ge(s) = [Gu(s) Gd(s) Gf(s); I 0 0]
R = gir(Q*[sysf;eye(mu,mu+md+mf)]); % form R(s)

% check decoupling condition and synthesis results
norm_rez = fdimmperf(R,Rf)

% check strong fault isolability
match_SFDI = isequal(SFDI,fdisspec(Rf))

% simulate fault step responses
outnames = strseq('r_',1:size(SFDI,1));
fnames = strseq('f_',1:mf);
Rf = set(Rf,'InputName',fnames,'OutputName',outnames);
step(Rf,2);
title('Step responses from the fault inputs')
ylabel('Residuals')