%% Example - Solution of an EMMP
clear variables
% model setup
s = tf('s'); % define the Laplace variable s
% enter Gu(s) 
Gu = [s/(s^2+3*s+2) 1/(s+2);
     s/(s+1) 0;
      0 1/(s+2)];
mu = size(Gu,2); mf = mu;

% set input groups with Gf = Gu
sysf = fdimodset(ss(Gu),struct('c',1:mu,'f',1:mu));

% setup reference model Mr(s)
Mr = fdimodset(ss(eye(mf)),struct('f',1:mf));

tol = 1.e-7;  % set tolerance for rank computations
tolmin = tol; % set tolerance for observability tests
sdeg = -1;    % set stability degree

% solve a strong EFDIP using EMMSYN
opts = struct('tol',tol,'tolmin',tolmin,'sdeg',sdeg,'minimal',false);
[Q,R,info] = emmsyn(sysf,Mr,opts);

% display results
minreal(tf(Q)), tf(info.M)

% check synthesis results: R(s) := Q(s)*Ge(s) = M(s)*[0 Mr(s)],
% with Ge(s) = [Gu(s) Gu(s); I 0]
Ge = [sysf;eye(mu,mu+mf)]; Rtemp = Q*Ge;
norm_rez = fdimmperf(Rtemp,info.M*Mr)
% check computed R(s)
norm_dif = fdimmperf(Rtemp,R)  

%% Example for separation of disturbance inputs from noise inputs}
clear variables
% define system with control, noise and  fault inputs
A = [-.8 0 0; 0 -.5 .6; 0 -.6 -.5];
Bu = [1 1;1 0;0 1]; Bw = [0 0;0 1;1 0]; Bf = Bu;
C = [0 1 1; 1 1 0];
Du = zeros(2,2); Dw = Du; Df = Du;
mu = 2; md = 0; mw = 2; mf = 2; % set input dimensions
% setup the model with noise inputs redefined as faults
sysf = ss(A,[Bu Bw Bf],C,[Du Dw Df]);
% set input groups
Inputs = struct('c',1:mu,'f',mu+(1:md+mw+mf));
sysf = fdimodset(sysf,Inputs);

% compute the achievable weak specifications
S_weak = fdigenspec(sysf)
% select specifications with nonzero trailing elements
S_red = S_weak(sum(S_weak(:,end-mf+1:end),2) > 0,:)
% select specifications with zeros in leading elements
for i = 1:md+mw
    S_red(S_red(:,i) == 0,:)
end

%% Example - Solution of an AFDP
clear variables
% Example 5.3 of (V,2017) (modified)
s = tf('s'); % define the Laplace variable s
mu = 1; mw = 1; p = 2; mf = mu+p; % set dimensions
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gw = [(s-1)/(s+2); 0];            % enter Gw(s)

% set input groups with Gf = [Gu I]
Inputs = struct('c',1:mu,'f',1:mu,'fs',1:p,'n',mu+(1:mw));
sysf = fdimodset(ss([Gu Gw]),Inputs);

% use preliminary design with  EFDSYN
[Q,R] = efdsyn(sysf);  % R(s) = [ Rf(s) Rw(s)]

% compute achieved initial gap: gap0
gap0 = fdif2ngap(R)

% compute the outer-inner factorization
[rwi,rwo] = goifac(R(:,'noise')); % Rw(s) = Rwo(s)*Rwi(s)

% update Q(s) <- inv(Rwo(s))*Q(s) and R(s) <- inv(Rwo(s))*R(s)
Q = minreal(rwo\Q); R = minreal(rwo\R);
tf(Q), tf(R(:,'faults')),   tf(R(:,'noise'))

% compute achieved final gap 
gap = fdif2ngap(R)

% check results: R(s) := Q(s)*Ge(s) = [0 Rf(s) Rw(s)],
% with Ge(s) = [Gu(s) Gf(s) Gw(s); I 0 0]
norm_dif = fdimmperf(Q*[sysf;eye(mu,mu+mf+mw)],R)

% check strong fault detectability
S_strong = fdisspec(R(:,'faults'))

% simulate step responses from fault and noise inputs
figure
inpnames = {'f_1','f_2','f_3','w'};
set(R,'InputName',inpnames,'OutputName','r');
step(R); ylabel('')
title('Step responses from fault and noise inputs')

%% Example - Solution of an AFDP using the funtion AFDSYN
clear variables
% Example 5.3 of (V,2017) (modified)
s = tf('s'); % define the Laplace variable s
mu = 1; mw = 1; p = 2; mf = mu+p; % set dimensions
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gw = [(s-1)/(s+2); 0];            % enter Gw(s)

% set input groups with Gf = [Gu I]
Inputs = struct('c',1:mu,'f',1:mu,'fs',1:p,'n',mu+(1:mw));
sysf = fdimodset(ss([Gu Gw]),Inputs);

% use preliminary design with  EFDSYN
[Q,R] = efdsyn(sysf);  % R(s) = [Rf(s) Rw(s)]

% compute achieved initial gap: gap0
gap0 = fdif2ngap(R)

% use design with  AFDSYN
[Q,R,info] = afdsyn(sysf);  % R(s) = [Rf(s) Rw(s)]

tf(Q), tf(R(:,'faults')),   tf(R(:,'noise'))

[info.gap gap0] % gap > gap0

% check results: R(s) := Q(s)*Ge(s) = [0 Rf(s) Rw(s)],
% with Ge(s) = [Gu(s) Gf(s) Gw(s); I 0 0]
norm_rez = fdimmperf(Q*[sysf;eye(mu,mu+mf+mw)],R)

% check strong fault detectability
S_strong = fdisspec(R)

% simulate step responses from fault and noise inputs
figure
inpnames = {'f_1','f_2','f_3','w'};
set(R,'InputName',inpnames,'OutputName','r');
step(R); ylabel('')
title('Step responses from fault and noise inputs')


%% Example for solving an AFDIP
clear variables
% Example 5.3 of (V,2017) (modified)
s = tf('s'); % define the Laplace variable s
mu = 1; mw = 1; p = 2; mf = mu+p; % set dimensions
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gw = [(s-1)/(s+2); 0];            % enter Gw(s)

% set input groups with Gf = [Gu I]
Inputs = struct('c',1:mu,'f',1:mu,'fs',1:p,'n',mu+(1:mw));
sysf = fdimodset(ss([Gu Gw]),Inputs);

% select S{FDI}
Smax = fdigenspec(sysf), SFDI = Smax(sum(Smax,2)==2,:)
nb = size(SFDI,1);

% set options for least order synthesis with EFDISYN
options = struct('tol',1.e-7,'smarg',-3,...
                 'sdeg',-3,'SFDI',SFDI);
[Q,R] = efdisyn(sysf,options);

% compute achieved initial gaps gap0
gap0 = fdif2ngap(R,[],SFDI);

% update initial synthesis
for i = 1:nb
    gam = norm(R{i}(:,'noise'),inf);
    if gam > 1.e-7
       % compute the outer-inner factorization of Rw{i}
       [~,rwo] = goifac(R{i}(:,'noise'));
       % update Q{i} and R{i}
       Q{i} = minreal(rwo\Q{i});
       R{i} = minreal(rwo\R{i});
    end
    tf(Q{i}),tf(R{i}(:,'faults')),tf(R{i}(:,'noise'))
end

% compute achieved final gaps gap
gap = fdif2ngap(R,[],SFDI);
format short e
[gap0 gap] % gap0 <= gap

% check R{i}(s) := Q{i}(s)*Ge(s) = [0 Rf{i}(s) Rw{i}(s)],
% with Ge(s) = [Gu(s) Gd(s) Gf(s); I 0 0]
Ge = [sysf;eye(mu,mu+mf+mw)]; 
Rtemp = cell(nb,1);
for i = 1:nb, Rtemp{i} = Q{i}*Ge; end
norm_rez = fdimmperf(Rtemp,R)

% check fault isolability of constant faults
S_strong = fdisspec(R) 
match_SFDI = isequal(SFDI,S_strong)

% simulate step responses from fault and noise inputs
Rtot = vertcat(R{:});
figure
inpnames = {'f_1','f_2','f_3','w'};
outnames = {'r_1','r_2','r_3'};
set(Rtot,'InputName',inpnames,'OutputName',outnames);
step(Rtot); ylabel('Residuals')
title('Step responses from fault and noise inputs')

%% Example for solving an AFDIP using the funtion AFDISYN
clear variables
% Example 5.3 of (V,2017) (modified)
s = tf('s'); % define the Laplace variable s
mu = 1; mw = 1; p = 2; mf = mu+p; % set dimensions
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gw = [(s-1)/(s+2); 0];            % enter Gw(s)

% set input groups with Gf = [Gu I]
Inputs = struct('c',1:mu,'f',1:mu,'fs',1:p,'n',mu+(1:mw));
sysf = fdimodset(ss([Gu Gw]),Inputs);

% select SFDI
S = fdigenspec(sysf); SFDI = S(sum(S,2)==2,:)
nb = size(SFDI,1);

% set options for least order synthesis with EFDISYN
options = struct('tol',1.e-7,'smarg',-3,...
                 'sdeg',-3,'SFDI',SFDI);
[Q,R] = efdisyn(sysf,options);

% compute achieved initial gaps gap0
gap0 = fdif2ngap(R,[],SFDI);

options = struct('tol',1.e-7,'smarg',-3,...
                 'sdeg',-3,'SFDI',SFDI);
[Q,R,info] = afdisyn(sysf,options);

% compare initial and final gaps
format short e
[gap0 info.gap] % gap0 <= gap

% check R{i}(s) := Q{i}(s)*Ge(s) = [0 Rf{i}(s) Rw{i}(s)],
% with Ge(s) = [Gu(s) Gd(s) Gf(s); I 0 0]
Ge = [sysf;eye(mu,mu+mf+mw)]; 
Rtemp = cell(nb,1);
for i = 1:nb, Rtemp{i} = Q{i}*Ge; end
norm_rez = fdimmperf(Rtemp,R)
    
% check fault isolability of constant faults
S_strong = fdisspec(R) 
match_SFDI = isequal(SFDI,S_strong)

% simulate step responses from fault and noise inputs
Rtot = vertcat(R{:});
figure
inpnames = {'f_1','f_2','f_3','w'};
outnames = {'r_1','r_2','r_3'};
set(Rtot,'InputName',inpnames,'OutputName',outnames);
step(Rtot); ylabel('Residuals')
title('Step responses from fault and noise inputs')


%% Example for solving an AMMP
clear variables
% Example - Solution of an AMMP
% Example 5.11 of (V,2017)
s = tf('s'); % define the Laplace variable s
% define Gu(s), Gd(s), Gf(s)
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gf = [(s+1)/(s+2) 0; 0 1];        % enter Gf(s)
Gw = [1/(s+2); 0];                % enter Gw(s)
mu = 1; mf = mu+1; mw = 1; p = 2; % set dimensions

% set input groups
Inputs = struct('c',1:mu,'f',mu+(1:mf), ...
                'n',mu+mf+(1:mw));
% build synthesis model with additive faults
sysf = fdimodset(ss([Gu Gf Gw]),Inputs)

% determine maximal achievable structure matrix
tol = 1.e-7; % set tolerance for rank computations
Smax = fdigenspec(sysf,struct('tol',tol))

% setup reference model Mr(s)
Mr = fdimodset(ss(eye(mf)),struct('f',1:mf));

% Step 1: compute left nullspace basis as Q = [I -Gu ]
% initialize R = [Rf Rw] := Q*[Gf Gw;0 0] 
Q = [eye(p) -sysf(:,'controls')]; R = sysf(:,{'faults','noise'}); 

% Step 3: quasi-co-outer-co-inner factorization
[Gi,Go,info] = goifac(R,tol); % [Rf Rw] = Go*Gi
info.nfuz+info.niuz    % Go has no unstable zeros 

% update Q <- Q3*Q, Rf <- Q3*Rf, Rw <- Q3*Rw with Q3 = inv(Go)
Q3 = inv(Go); Q = minreal(Q3*Q,tol); R = minreal(Q3*R,tol); 

% compute F = [F1 F2] =  [Mr 0]*Gi'
F = [Mr zeros(mf,mw)]*Gi'; 

% Step 4: solve the H_inf-LDP min||[F1-Q4  F2] ||_inf
options = struct('tol',tol,'reltol',5.e-8);
[Q4,gopt] = glinfldp(F,mw,options); gopt
Q = Q4*Q;  R = Q4*R;    %  update Q <- Q4*Q, R <- Q4*R 

% display results
minreal(zpk(Q))
Rf = minreal(zpk(R(:,'faults')))
Rw = minreal(zpk(R(:,'noise')))

% compute achieved performance
gammaopt = fdimmperf(R,Mr)

% check decoupling condition and synthesis results
norm_rez  = fdimmperf(Q*[sysf;eye(mu,mu+mf+mw)],R)

%% Example for solving an AMMP using the function AMMSYN
clear variables
% Example - Solution of an AMMP
% Example 5.11 of (V,2017)
s = tf('s'); % define the Laplace variable s
% define Gu(s), Gd(s), Gf(s)
Gu = [(s+1)/(s+2); (s+2)/(s+3)];  % enter Gu(s)
Gf = [(s+1)/(s+2) 0; 0 1];        % enter Gf(s)
Gw = [1/(s+2); 0];                % enter Gw(s)
mu = 1; mf = mu+1; mw = 1; p = 2; % set dimensions

% set input groups
Inputs = struct('c',1:mu,'f',mu+(1:mf), ...
                'n',mu+mf+(1:mw));
% build synthesis model with additive faults
sysf = fdimodset(ss([Gu Gf Gw]),Inputs)

% determine maximal achievable structure matrix
tol = 1.e-7; % set tolerance for rank computations
Smax = fdigenspec(sysf,struct('tol',tol))

% setup reference model Mr(s)
Mr = fdimodset(ss(eye(mf)),struct('f',1:mf));

options = struct('tol',tol,'reltol',5.e-8);
[Q,R,info] = ammsyn(sysf,Mr,options);

% display results
minreal(zpk(Q)),  tf(info.M)
Rf = minreal(zpk(R(:,'faults'))), Rw = minreal(zpk(R(:,'noise')))
info.gammaopt

% check decoupling condition and synthesis results
Ge = [sysf;eye(mu,mu+mf+mw)]; Rtemp = gminreal(Q*Ge);
norm_rez  = fdimmperf(Rtemp,R)

% check achieved synthesis performance 
gammaopt = fdimmperf(Rtemp,Mr)


