%% Example - Solution of an EMDP
clear variables
% Example 6.1 of (V,2017) 
% model setup: lateral aircraft model without faults
A = [-.4492 .046  .0053 -.9926;
       0    0     1      .0067;
   -50.8436 0   -5.2184  .722;
    16.4148 0     .0026 -.6627];
Bu = [.0004 .0011; 0 0; -1.4161 .2621; -.0633 -.1205];
C = eye(4); p = size(C,1); mu = size(Bu,2);
% define the LOE faults Gamma(j,:), j = 1, ..., 9
Gamma = 1 - [ 0  0 0 .5 .5 .5 1  1 1;
              0 .5 1  0 .5  1 0 .5 1 ]';
N = size(Gamma,1);
% define multiple fault models: Gu{j} = Gu*diag(Gamma(j,:)), j = 1, ..., N
sysu = ss(zeros(p,mu,N,1));
for j = 1:N
    sysu(:,:,j,1) = ss(A,Bu*diag(Gamma(j,:)),C,0); 
end
% set input groups
sysu = mdmodset(sysu,struct('c',1:mu));

% analysis of nu-gap based distances between component models
nugapdist = mddist(sysu);
figure, mesh(nugapdist), colormap hsv
title('\nu-gap distances between component models')
ylabel('Model numbers')
xlabel('Model numbers')

% analysis of H-inf-norm based distances between component models
hinfdist = mddist(sysu,struct('distance','Inf'));
figure, mesh(hinfdist), colormap hsv
title('H_\infty norm based distances between component models')
ylabel('Model numbers')
xlabel('Model numbers')

% solve an EMDP using EMDSYN

% enforce the same design matrix for all filters 
H = [ 0.7645 0.8848 0.5778 0.9026 ];
opt_emdsyn = struct('sdeg',-1,'poles',-1,'HDesign',H);
[Q,R,info] = emdsyn(sysu,opt_emdsyn); info.MDperf

% check synthesis results: R{i,j} = Q{i}*Ge{j},
% with Ge{j} = [Gu{j}; I]
syse = [sysu;eye(mu)]; S = false(N); 
fail = true;
for i = 1:N
    for j = 1:N 
        Rij = Q{i}*syse(:,:,j,1); 
        S(i,j) = norm(Rij,inf) > 1.e-5;
        fail = fail && norm(Rij-R{i,j},inf) > 1.e-5;
    end
end
S, fail

% check computed performance:
norm_diff = norm(info.MDperf-mdperf(R))

% commands to generate the figures for Example 6.1
figure
mesh(info.MDperf)
colormap hsv
title('Distance mapping performance')
ylabel('Residual numbers')
xlabel('Model numbers')

figure
for i = 1:N, ylab{i} = ['r^(^',num2str(i),'^)']; end
k1 = 0;
for j = 1:N
  k1 = k1+1;
  k = k1;
  for i=1:N
    [r,t] = step(R{j,i},4);
    subplot(N,N,k)
    plot(t,r(:,:,1),t,r(:,:,2)),
    if i == 1, title(['Model ',num2str(j)]), end
    if i == j, ylim([-1 1]), end
    if j == 1, ylabel(ylab{i},'FontWeight','bold'), end
    if i == N && j == 5 
       xlabel('Time (seconds)','FontWeight','bold')
    end
    k = k+N;
  end
end


%% Example - Solution of an AMDP
clear variables
% Example 6.2 of (V,2017) 
% model setup: lateral aircraft model without faults
A = [-.4492 .046  .0053 -.9926;
       0    0     1      .0067;
   -50.8436 0   -5.2184  .722;
    16.4148 0     .0026 -.6627];
Bu = [.0004 .0011; 0 0; -1.4161 .2621; -.0633 -.1205];
[n,mu] = size(Bu); p = 2; mw = n+p; m = mu+mw;
Bw = eye(n,mw);
C = 180/pi*eye(p,n);  Du = zeros(p,mu); Dw = [zeros(p,n) eye(p)];
% define the LOE faults Gamma(j,:), j = 1, ..., 9
Gamma = 1 - [ 0  0 0 .5 .5 .5 1  1 1;
            0 .5 1  0 .5  1 0 .5 1 ]';
N = size(Gamma,1);
% define multiple fault models: Gu{j} = Gu*diag(Gamma(j,:)), j = 1, ..., N
sysuw = ss(zeros(p,mu+mw,N,1));
for j = 1:N
    sysuw(:,:,j,1) = ss(A,[Bu*diag(Gamma(j,:)) Bw],C,[Du Dw]); 
end
% set input groups
sysuw = mdmodset(sysuw,struct('c',1:mu,'n',mu+(1:mw)));

% use preliminary (nonminimal) design with  EMDSYN
opt_emdsyn = struct('sdeg',-1,'poles',-1,'minimal',false);
[Q,R,info] = emdsyn(sysuw,opt_emdsyn); info.MDperf

% compute achieved initial gaps
gap0 = mdgap(R);

% perform optimal synthesis (standard case)
tol = 1.e-7; distinf = zeros(N);
for i = 1:N
  [gi,go] = goifac(R{i,i}(:,'noise'),1.e-7);
  Q{i} = gminreal(go\Q{i},tol);
  for j = 1:N
    warning('off','all')
    R{i,j} = gir(Q{i}*[sysuw(:,:,j,1); eye(mu,mu+mw)],tol);
    warning('on','all')
    distinf(i,j) = norm(R{i,j}(:,'controls'),inf);
  end
end

% scale Q^{i} and R^{i,j}; determine gaps
for i=1:N
  scale = min(distinf(i,[1:i-1 i+1:N]));
  distinf(i,:) = distinf(i,:)/scale;
  Q{i} = Q{i}/scale;
  for j = 1:N
     R{i,j} = R{i,j}/scale;
  end
end
% compute achieved optimal gaps
gap = mdgap(R);
[gap0,gap]

% commands to generate the figures for Example 6.2
figure, mesh(distinf)
colormap hsv
% colormap winter
% colormap parula
title('Distance mapping performance')
ylabel('Residual numbers')
xlabel('Model numbers')

% generate input signals for Ex. 6.2
d = diag([ 1 1 0.01 0.01 0.01 0.01 0.03 0.03]);
t = (0:0.01:10)';  ns = length(t);
usin = gensig('sin',pi,t(end),0.01)+1.5;
usquare = gensig('square',pi*2,t(end),0.01)+0.3;
u = [ usquare usin (rand(ns,mw)-0.5)]*d;  
 
%
figure
k1 = 0; alpha = 0.9; beta = 0.1; gamma = 10;
for j = 1:N
  k1 = k1+1; k = k1;
  for i=1:N 
    subplot(N,N,k), 
    [r,t] = lsim(R{j,i},u,t);
    % use a Narendra filter with (alpha,beta,gamma) = (0.9,0.1,10)
    theta = alpha*sqrt(r(:,1).^2+r(:,2).^2)+...
        beta*sqrt(lsim(tf(1,[1 gamma]),r(:,1).^2+r(:,2).^2,t));
    plot(t,real(theta)), 
    if i == 1, title(['Model ',num2str(j)]), end
    if i == j, ylim([0 4]), end 
    if j == 1, ylabel(['\theta_', num2str(i)],'FontWeight','bold'), end
    if i == N && j == 5, xlabel('Time (seconds)','FontWeight','bold'), end
    k = k+N;
  end
end

%% Example - Solution of an AMDP using the function AMDSYN
clear variables
% Example 6.2 of (V,2017) 
% model setup: lateral aircraft model without faults
A = [-.4492 .046  .0053 -.9926;
       0    0     1      .0067;
   -50.8436 0   -5.2184  .722;
    16.4148 0     .0026 -.6627];
Bu = [.0004 .0011; 0 0; -1.4161 .2621; -.0633 -.1205];
[n,mu] = size(Bu); p = 2; mw = n+p; m = mu+mw;
Bw = eye(n,mw);
C = 180/pi*eye(p,n);  Du = zeros(p,mu); Dw = [zeros(p,n) eye(p)];
% define the LOE faults Gamma(j,:), j = 1, ..., 9
Gamma = 1 - [ 0  0 0 .5 .5 .5 1  1 1;
            0 .5 1  0 .5  1 0 .5 1 ]';
N = size(Gamma,1);
% define multiple fault models: Gu{j} = Gu*diag(Gamma(j,:)), j = 1, ..., N
sysuw = ss(zeros(p,mu+mw,N,1));
for j = 1:N
    sysuw(:,:,j,1) = ss(A,[Bu*diag(Gamma(j,:)) Bw],C,[Du Dw]); 
end
% set input groups
sysuw = mdmodset(sysuw,struct('c',1:mu,'n',mu+(1:mw)));

% use preliminary (nonminimal) design with  EMDSYN
opt_emdsyn = struct('sdeg',-1,'poles',-1,'minimal',false);
[Q,R,info0] = emdsyn(sysuw,opt_emdsyn); info0.MDperf

%
% compute achieved initial gaps
gap0 = mdgap(R);

% use nonminimal design with  AMDSYN
opt_amdsyn = struct('sdeg',-1,'poles',-1,'minimal',false);
[Q,R,info] = amdsyn(sysuw,opt_amdsyn); info.MDperf


% achieved initial and optimal gaps 
[gap0 info.MDgap]
%

% commands to generate the figures for Example 6.2
figure, mesh(info.MDperf)
colormap hsv
title('Distance mapping performance')
ylabel('Residual numbers')
xlabel('Model numbers')

% generate input signals for Ex. 6.2
d = diag([ 1 1 0.01 0.01 0.01 0.01 0.03 0.03]);
t = (0:0.01:10)';  ns = length(t);
usin = gensig('sin',pi,t(end),0.01)+1.5;
usquare = gensig('square',pi*2,t(end),0.01)+0.3;
u = [ usquare usin (rand(ns,mw)-0.5)]*d;  
 
%
figure
k1 = 0; alpha = 0.9; beta = 0.1; gamma = 10;
for j = 1:N
  k1 = k1+1; k = k1;
  for i=1:N 
    subplot(N,N,k), 
    [r,t] = lsim(R{j,i},u,t);
    % use a Narendra filter with (alpha,beta,gamma) = (0.9,0.1,10)
    theta = alpha*sqrt(r(:,1).^2+r(:,2).^2)+...
        beta*sqrt(lsim(tf(1,[1 gamma]),r(:,1).^2+r(:,2).^2,t));
    plot(t,real(theta)), 
    if i == 1, title(['Model ',num2str(j)]), end
    if i == j, ylim([0 4]), end 
    if j == 1, ylabel(['\theta_', num2str(i)],'FontWeight','bold'), end
    if i == N && j == 5, xlabel('Time (seconds)','FontWeight','bold'), end
    k = k+N;
  end
end

