# **MATLAB exercise scripts for a graduate course on: Model-Based Fault Diagnosis - a Linear Synthesis Framework using MATLAB **  #

## About 

`FDIBOOK_COURSE_EXERCISES` is a collection of MATLAB scripts and live scripts with exercises developed in conjunction with a graduate course on:

   -  Model-Based Fault Diagnosis - a Linear Synthesis Framework using MATLAB. 

The current release of the `FDIBOOK_COURSE_EXERCISES` is version 1.3.1 (last updated by February 15, 2021).

## Requirements

The scripts have been developed under MATLAB 2015b and have been tested with MATLAB 2016a through 2020b. The live scripts have been developed under MATLAB 2017b and have been tested with MATLAB 2018a through 2020b. To use the scripts, the Control System Toolbox, the Robust Control Toolbox, the Descriptor System Tools collection - [`DSTOOLS`](https://bitbucket.org/DSVarga/dstools) (Version V0.75 or later), and the Fault Detection and Isolation Tools collection - [`FDITOOLS`](https://bitbucket.org/DSVarga/fditools) (Version V1.0 or later), must be installed in MATLAB running under 64-bit Windows 7, 8, 8.1 or 10.

## License

See `license.txt` for licensing information.

## Installing and executing the software ##

To install and execute the exercise scripts, perform the following steps:

* create on your computer a directory (say `exercises`)
* copy the sripts in the directory `exercises`
* start MATLAB and change within MATLAB, to the `exercises` directory
* run any script by typing its name into the command line

Each exercise script has several parts, which can be separately executed using the MATLAB Editor.  

## Contact information ##

For questions or  problems encountered, please contact Andreas Varga (email: varga.andreas@gmail.com).  

## Course hystory ##

The first course was held by Andreas Varga, with the help of Daniel Ossmann,  at the Universite Paris Saclay in the period  March 19-23, 2018.  The course was organized by the European Embedded Control Institute (EECI) in the framework of the International Graduate School on Control (IGSC) Program (Prof. F. Lamnabhi-Lagarrigue).

A second course was held by Andreas Varga, with the help of Daniel Ossmann,  at the Technical University of Munich in the period  April 4-25, 2018. The course was organized by the Institute of Flight System Dynamics (Prof. F. Holzapfel).

A third course was joinly held by Andreas Varga and Daniel Ossmann,  at the University of Padova in the period  March 11-15, 2019. The course was organized by the European Embedded Control Institute (EECI) in the framework of the International Graduate School on Control (IGSC) Program (Prof. F. Lamnabhi-Lagarrigue).
